package swgui;

import java.awt.BorderLayout;
import javax.swing.JFrame;

public class SWGui extends JFrame {

    public SWGui(){
        super("Smith - Waterman Patern Finder");
        StartPanel startPanel = new StartPanel();
        SelectPanel selectPanel = new SelectPanel(startPanel);
        RadioButtonsPanel radioPanel = new RadioButtonsPanel(startPanel);
        
        setLayout(new BorderLayout());
        add(selectPanel, BorderLayout.NORTH);
        add(radioPanel, BorderLayout.CENTER);
        add(startPanel, BorderLayout.SOUTH);
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 140);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        SWGui gui = new SWGui();
    }
}