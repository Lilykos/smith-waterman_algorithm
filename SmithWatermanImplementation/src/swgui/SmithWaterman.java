package swgui;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class SmithWaterman {
    
    private String str1;
    private String str2;
    private int len1;
    private int len2;
    
    static final int MATCH = 10;
    static final int MISMATCH = -8;
    static final int NEW_SPACE = -8;
    static final int EXTEND_SPACE = -1;
    
    static final int LEFT = 1;
    static final int UP = 2;
    static final int DIAG = 3;
    static final int ZERO = 0;
    static final int LIMIT = 20;
    
    private int[][] scoreBoard;
    private int[][] rowResults;
    private int[][] columnResults;
    private int[][] path;
    
    public SmithWaterman(String s1, String s2, int threads) {
        int i;
        str1 = s1;
        str2 = s2;
        len1 = str1.length();
        len2 = str2.length();
        
        try {
            scoreBoard = new int[len1+1][len2+1];
            rowResults = new int[len1+1][len2+1];
            columnResults = new int[len1+1][len2+1];
            path = new int[len1+1][len2+1];
        } catch (OutOfMemoryError e) {
            callMemErrorFrame();
        }
        
        for(i = 0; i <= len1; i++) {
            scoreBoard[i][0] = 0;
            rowResults[i][0] = 0;
            columnResults[i][0] = 0;
            path[i][0] = 0;
        }
        
        for(i = 1;i <= len2; i++) {
            scoreBoard[0][i] = 0;
            rowResults[0][i] = 0;
            columnResults[0][i] = 0;
            path[0][i] = 0;
        }
    }
    
    public String[] run(){
        int i, j, upperLeft;
        
        for(i = 1; i <= len1; i++) {
            for(j = 1; j <= len2; j++) {
                rowResults[i][j] = Math.max(rowResults[i][j - 1] + EXTEND_SPACE, scoreBoard[i][j - 1] + NEW_SPACE);
                columnResults[i][j] = Math.max(columnResults[i - 1][j] + EXTEND_SPACE, scoreBoard[i - 1][j] + NEW_SPACE);
                
                upperLeft = scoreBoard[i - 1][j - 1] + compare(i, j);
                scoreBoard[i][j] = 0;
                path[i][j] = ZERO;
                
                if(rowResults[i][j] >= scoreBoard[i][j]) {
                    scoreBoard[i][j] = rowResults[i][j];
                    path[i][j] = LEFT;
                }
                if(columnResults[i][j] >= scoreBoard[i][j]) {
                    scoreBoard[i][j] = columnResults[i][j];
                    path[i][j] = UP;
                }
                if(upperLeft >= scoreBoard[i][j]) {
                    scoreBoard[i][j] = upperLeft;
                    path[i][j] = DIAG;
                }
            }
        }
        
        rowResults=null;
        columnResults=null;
        
        System.gc();
      
        ArrayList<Sequence> ar = findSequences();
        int limit;      
        limit = ar.size();
        
        if(limit > 10) { limit=10; }
        
        String[] result;
        result = new String[limit*2];
        
        for(i = 0; i < limit; i++) {
            result[2*i] = ar.get(i).str1;
            result[2*i+1] = ar.get(i).str2;
        }
        
        scoreBoard=null;
        path=null;
        
        System.gc();
        
        return result;
    }
      
   private int compare(int i, int j) {
       if(str1.charAt(i - 1) == str2.charAt(j - 1)) {
           return MATCH;
       }
       return MISMATCH;
   }
   
   private String[] getMatch(int maxI, int maxJ) {
        int i = maxI;
        int j = maxJ;
        StringBuffer temp1, temp2;
        
        temp1 = new StringBuffer("");
        temp2 = new StringBuffer("");
        while(path[i][j] != ZERO) {
            if(path[i][j] == DIAG) {
                temp1.insert(0, str1.charAt(i - 1));
                temp2.insert(0, str2.charAt(j - 1));
                i--;
                j--;
            }
            else if(path[i][j] == UP) {
                temp1.insert(0, str1.charAt(i - 1));
                temp2.insert(0, '_');
                i--;
            }
            else if(path[i][j] == LEFT) {
                temp1.insert(0, '_');
                temp2.insert(0, str2.charAt(j - 1));
                j--;
            }
        }
       
        String[] result;
        result = new String[2];
        result[0] = temp1.toString();
        result[1] = temp2.toString();
        
        return result;
   }
   
   private ArrayList<Sequence> findSequences() {
       ArrayList<Sequence> ar = new ArrayList<>();
       
       int flag;
       for (int i = 1; i <= len1; i++) {
           for (int j = 1; j <= len2; j++) {
               if (scoreBoard[i][j] > LIMIT && scoreBoard[i][j]>scoreBoard[i - 1][j - 1]
                       && scoreBoard[i][j]>scoreBoard[i - 1][j] && scoreBoard[i][j]>scoreBoard[i][j - 1]) {
                   
                   if (i == len1 || j == len2 ||  scoreBoard[i][j] > scoreBoard[i + 1][j + 1]) {
                        String[] temp = getMatch(i, j);
                        int k = 0;
                        if(ar.isEmpty()) {
                            ar.add(new Sequence(temp[0], temp[1], scoreBoard[i][j]));
                        } else {
                            flag = 0;
                            while(k < ar.size()) {
                                if(ar.get(k).score < scoreBoard[i][j]) {
                                    ar.add(k, new Sequence(temp[0], temp[1], scoreBoard[i][j]));
                                    flag = 1;
                                    break;
                                }
                                k++;
                            }
                            if(flag == 0) {
                                ar.add(k, new Sequence(temp[0], temp[1], scoreBoard[i][j]));
                            }
                        }
                    }
                }
            }
       }
       return ar;
   }
   
   private void callMemErrorFrame() {
       JFrame patterns = new JFrame("Out of Memory!");
       JTextArea txtArea = new JTextArea();
       
       Color color = new Color(238, 233, 233);
       Font font = new Font("Verdana", Font.BOLD, 14);
       txtArea.setFont(font);
       txtArea.setForeground(Color.RED);
       txtArea.setBackground(color);
       txtArea.setText("Out of Memory Error! Please select different string length and try again!");
        
       patterns.add(txtArea);
       patterns.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
       patterns.pack();
       patterns.setLocation(450, 500);
       patterns.setVisible(true);
   }
}

class Sequence {
    
    public String str1;
    public String str2;
    public int score;
    
    public Sequence(String st1, String st2, int sc) {
        str1 = st1;
        str2 = st2;
        score = sc;
    }
}