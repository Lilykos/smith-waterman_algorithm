package swgui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

public class SelectPanel extends JPanel {
    
    private JTextField txtField;
    private JButton chooseButton;
    private JTextField threadField;
    private JButton threadButton;
    
    public StartPanel startPanel;
    private BufferedReader reader;
    private BufferedReader threadReader;
    
    public SelectPanel(StartPanel startPanel) {
        this.startPanel = startPanel;
        
        txtField = new JTextField();
        txtField.setPreferredSize(new Dimension(160, 25));
        ChooseHandler handler = new ChooseHandler();
        chooseButton = new JButton("Choose File");
        chooseButton.addActionListener(handler);
        
        
        threadField = new JTextField();
        threadField.setPreferredSize(new Dimension(50, 25));
        ThreadHandler threadHandler = new ThreadHandler();
        threadButton = new JButton("Confirm Thread Num.");
        threadButton.addActionListener(threadHandler);
        
        add(txtField);
        add(chooseButton);
        add(threadField);
        add(threadButton);
    }
    
    public class ThreadHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            String t = threadField.getText();
             try { 
                int threads = Integer.parseInt(t);
                startPanel.setThreadNumber(threads);
            } catch(NumberFormatException e) { 
                callWrongThreadFrame(); 
            }
        }

        private void callWrongThreadFrame() {
            JFrame patterns = new JFrame("Thread Input:");
            JTextArea txtArea = new JTextArea();
            
            Color color = new Color(238, 233, 233);
            Font font = new Font("Verdana", Font.BOLD, 14);
            txtArea.setFont(font);
            txtArea.setForeground(Color.RED);
            txtArea.setBackground(color);
            txtArea.setText("Wrong Input for threads! Try numerical integer values!");

            patterns.add(txtArea);
            patterns.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            patterns.pack();
            patterns.setLocation(450, 500);
            patterns.setVisible(true);
        }
    }
    
    
    public class ChooseHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt");
            chooser.setFileFilter(filter);
            
            int result = chooser.showOpenDialog(null);
            if(result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();
                txtField.setText(selectedFile.getAbsolutePath());
                
                try {
                    reader = new BufferedReader(new FileReader(selectedFile));
                    String st;
                    String finalString = "";
                    try {
                        while((st = reader.readLine()) != null) {
                            finalString = finalString + st;
                        } 
                        startPanel.setFile(finalString);
                    } catch (IOException ex) { ex.printStackTrace(); }
                    
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                    
                } finally {
                    try { reader.close();
                    } catch (IOException ex) { ex.printStackTrace(); }
                }
            }
        } 
    }
}
