package swgui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

public class StartPanel extends JPanel {
    
    private JButton startButton;
    private int length = 50;
    private String file = "";
    private SmithWaterman swMatcher;
    private String[] results; 
    private int threads = 0;
    
    public StartPanel() {
        StartListener start = new StartListener();
        startButton = new JButton("Start!");
        startButton.addActionListener(start);
        add(startButton);
    }
    
    public void setLength(int length) {
        this.length = length;
    }
    
    public void setFile(String file) {
        this.file = file;
    }
    
    public void setThreadNumber(int threads) {
        this.threads = threads;
    }
    
    public class StartListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            String seq1 = null, seq2 = null;
            
            if (checkForMistakes(length, file, threads)) {
                seq1 = getSequence(file, length);
                seq2 = getSequence(file, length);
                
                long time1 = System.currentTimeMillis();
                
                swMatcher = new SmithWaterman(seq1, seq2, threads);
                results = swMatcher.run();
                
                long time2 = System.currentTimeMillis();
                callPaternsFrame(results, time2 - time1, seq1, seq2);
            } else {
                callMistakeFrame();
            }
        }    

        private boolean checkForMistakes(int length, String file, int threads1) {
            if (file.length() < length) {
                return false;
            }
            for (int i = 0; i < file.length(); i++) {
                if (file.charAt(i) != 'G' && file.charAt(i) != 'A'
                        && file.charAt(i) != 'T' && file.charAt(i) != 'C') {
                    return false;
                }
            }
            return true;
        }

        private void callMistakeFrame() {
            JFrame patterns = new JFrame("Pattern Matching:");
            JTextArea txtArea = new JTextArea();
            
            Color color = new Color(238, 233, 233);
            Font font = new Font("Verdana", Font.BOLD, 14);
            txtArea.setFont(font);
            txtArea.setForeground(Color.RED);
            txtArea.setBackground(color);
            txtArea.setText("Unable to match patterns! Possible mistake on data file!");

            patterns.add(txtArea);
            patterns.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            patterns.pack();
            patterns.setLocation(450, 500);
            patterns.setVisible(true);
        }

        private void callPaternsFrame(String[] results, long time, String seq1, String seq2) {
            JFrame patterns = new JFrame("Pattern Matching:");
            ColorPane pane = new ColorPane();
            pane.setText("");
            
            int matches = results.length;
            char[] res1, res2;
            String s;
            
            pane.append(Color.BLACK, "Original sequence 1: " + seq1 + "\n");
            pane.append(Color.BLACK, "Original sequence 2: " + seq2 + "\n\n");
            pane.append(Color.BLACK, "Pattern Matcing Comparison:\n\n");
            
            for (int j = 0; j < matches - 1; j = j + 2) {
                res1 = results[j].toCharArray();
                res2 = results[j + 1].toCharArray();
                length = results[j].length();
                
                for (int i = 0; i < length; i++) {
                    s = String.valueOf(res1[i]);
                    if (res1[i] == res2[i]) {
                        pane.append(Color.RED, s + " ");
                    } else {
                        pane.append(Color.BLACK, s + " ");
                    }
                }
                pane.append(Color.BLACK, "\n");
                
                for (int i = 0; i < length; i++) {
                    s = String.valueOf(res2[i]);
                    if (res1[i] == res2[i]) {
                        pane.append(Color.RED, s + " ");
                    } else {
                        pane.append(Color.BLACK, s + " ");
                    }
                }
                pane.append(Color.BLACK, "\n\n");
            }
            pane.append(Color.BLUE, "Time needed in milliseconds:" + String.valueOf(time));
            pane.setEditable(false);
            
            JScrollPane jScrollPane = new JScrollPane(pane);
            jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            
            patterns.setContentPane(jScrollPane);
            patterns.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            patterns.setLocationRelativeTo(null);
            patterns.setExtendedState(JFrame.MAXIMIZED_BOTH);
            patterns.setVisible(true);
        }

        private String getSequence(String file, int length) {
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(file.length() - length);
            
            return file.substring(randomInt, randomInt + length);
        }
        
        
        public class ColorPane extends JTextPane {
            
            public void append(Color c, String s) {
                StyleContext sc = StyleContext.getDefaultStyleContext();
                AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);
                int len = getDocument().getLength();
                setCaretPosition(len);
                setCharacterAttributes(aset, false);
                replaceSelection(s);
            }
        }
    }
}