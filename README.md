SmithWatermanAlgorithm
======================

An implementation of the Smith-Waterman Algorithm for pattern matching, in Java. In our case it is used for matching dna patterns (data samples included), using a simple GUI for choosing and error-checking the data files and presenting the results.<br><br>
Also, a patch which implements parallelism in the algorithm is included.
