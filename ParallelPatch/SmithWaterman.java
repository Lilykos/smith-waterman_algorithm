package swgui;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JTextArea;


public class SmithWaterman {
    
    private final String str1;
    private final String str2;
    private int len1;
    private int len2;
    
    private int maxThreads;
        
    static final int LEFT = 1;
    static final int UP = 2;
    static final int DIAG = 3;
    static final int ZERO = 0;
    static final int LIMIT = 20;
    
    private int[][] scoreBoard;
    private int[][] rowResults;
    private int[][] columnResults;
    private int[][] path;
    
    public SmithWaterman(String s1, String s2, int parallelism){
        int i;
        str1 = s1;
        str2 = s2;
        len1 = str1.length();
        len2 = str2.length();
        maxThreads = parallelism;
        
        if(maxThreads > len1 || maxThreads > len2) {
            maxThreads = Math.min(len1, len2);
            System.out.println("Parallelism degree asked was too high! Reducing to " + maxThreads);
        } else if(maxThreads < 2) {
            maxThreads = 2;
            System.out.println("Parallelism degree asked was too low! Increasing to minimum value " + maxThreads);
        }
        
        try {
            scoreBoard = new int[len1+1][len2+1];
            rowResults = new int[len1+1][len2+1];
            columnResults = new int[len1+1][len2+1];
            path = new int[len1+1][len2+1];
        } catch (OutOfMemoryError e) {
            callMemErrorFrame();
        }
        
        for(i = 0; i <= len1; i++) {
            scoreBoard[i][0] = 0;
            rowResults[i][0] = 0;
            columnResults[i][0] = 0;
            path[i][0] = ZERO;
        }
        
        for(i = 1; i <= len2; i++) {
            scoreBoard[0][i] = 0;
            rowResults[0][i] = 0;
            columnResults[0][i] = 0;
            path[0][i] = ZERO;
        }
    }
    
    
    public String[] run() {
        int i, j, div1, div2;
        
        ArrayList<Thread> threads = new ArrayList<Thread>();
        
        div1 = len1/maxThreads;
        div2 = len2/maxThreads;
        
        Bounds[][] bounds = new Bounds[maxThreads][maxThreads];
        
        for(i = 0; i < maxThreads; i++) {
            for(j = 0; j < maxThreads; j++) {
                if(i == maxThreads - 1 && j == maxThreads - 1) {
                    bounds[i][j] = new Bounds(i*div1 + 1, len1, j*div2 + 1, len2);
                } else if(i == maxThreads - 1) {
                    bounds[i][j] = new Bounds(i*div1 + 1, len1, j*div2 + 1, (j + 1)*div2);
                } else if(j == maxThreads - 1) {
                    bounds[i][j] = new Bounds(i*div1 + 1,(i + 1)*div1, j*div2 + 1, len2);
                } else {
                    bounds[i][j] = new Bounds(i*div1 + 1, (i + 1)*div1, j*div2 + 1, (j + 1)*div2);
                }
            }
        }
        
        for(i = 0; i < maxThreads; i++) {
            for(j = 0;j <= i; j++) {
                Thread t = new Thread(new ScoreBoardThread(scoreBoard, rowResults,
                        columnResults, path, bounds[j][i - j], str1, str2));
                threads.add(t);
                t.start();
            }
            for(j = 0; j <= i; j++) {
                try {
                    threads.get(0).join();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } finally{
                    threads.remove(0);
                }
            }
        }
        
        for(i = 0; i < maxThreads - 1; i++) {
            for(j = i + 1; j < maxThreads; j++) {
                Thread t = new Thread(new ScoreBoardThread(scoreBoard, rowResults,
                        columnResults, path, bounds[j][maxThreads + i - j], str1, str2));
                threads.add(t);
                t.start();
            }
            for(j = 0; j < maxThreads - (i + 1); j++) {
                try {
                    threads.get(0).join();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } finally{
                    threads.remove(0);
                }
            }
        }
        
        rowResults = null;
        columnResults = null;
        
        System.gc();

        ArrayList<Sequence> ar = findSequences();
        int limit;
                
        limit = ar.size();
        if(limit > 10) { limit=10; }
        
        String[] result;
        result = new String[limit*2];
        
        for(i = 0; i < limit; i++) {
            result[2*i] = ar.get(i).str1;
            result[2*i + 1] = ar.get(i).str2;
        }
        
        scoreBoard = null;
        path = null;
        
        System.gc();
        
        return result;        
   }
    
    private String [] getMatch(int maxI, int maxJ) {
        int i = maxI;
        int j = maxJ;
        
        StringBuffer temp1, temp2;
        temp1 = new StringBuffer("");
        temp2 = new StringBuffer("");
        while(path[i][j] != ZERO) {
            if(path[i][j] == DIAG) {
                temp1.insert(0 ,str1.charAt(i - 1));
                temp2.insert(0, str2.charAt(j - 1));
                i--;
                j--;
            } else if(path[i][j] == UP) {
                temp1.insert(0, str1.charAt(i - 1));
                temp2.insert(0, '_');
                i--;
            } else if(path[i][j] == LEFT) {
                temp1.insert(0, '_');
                temp2.insert(0, str2.charAt(j - 1));
                j--;
            }
        }
       
        String[] result;
        result = new String[2];
        result[0]=temp1.toString();
        result[1]=temp2.toString();
        
        return result;
   }
   
   private ArrayList<Sequence> findSequences() {
       ArrayList<Sequence> ar = new ArrayList<>();
       
       int flag;
       for (int i = 1; i <= len1; i++) {
           for (int j = 1; j <= len2; j++) {
               if (scoreBoard[i][j] > LIMIT && scoreBoard[i][j]>scoreBoard[i - 1][j - 1]
                       && scoreBoard[i][j]>scoreBoard[i - 1][j] && scoreBoard[i][j]>scoreBoard[i][j - 1]) {
                   
                   if (i == len1 || j == len2 ||  scoreBoard[i][j] > scoreBoard[i + 1][j + 1]) {
                        String[] temp = getMatch(i, j);
                        int k = 0;
                        if(ar.isEmpty()) {
                            ar.add(new Sequence(temp[0], temp[1], scoreBoard[i][j]));
                        } else {
                            flag = 0;
                            while(k < ar.size()) {
                                if(ar.get(k).score < scoreBoard[i][j]) {
                                    ar.add(k, new Sequence(temp[0], temp[1], scoreBoard[i][j]));
                                    flag = 1;
                                    break;
                                }
                                k++;
                            }
                            if(flag == 0) {
                                ar.add(k, new Sequence(temp[0], temp[1], scoreBoard[i][j]));
                            }
                        }
                    }
                }
            }
       }
       return ar;
   }  
   private void callMemErrorFrame() {
     JFrame patterns = new JFrame("Out of Memory!");
     JTextArea txtArea = new JTextArea();
     
     Color color = new Color(238, 233, 233);
     Font font = new Font("Verdana", Font.BOLD, 14);
     txtArea.setFont(font);
     txtArea.setForeground(Color.RED);
     txtArea.setBackground(color);
     txtArea.setText("Out of Memory Error! Please select different string length and try again!");
     
     patterns.add(txtArea);
     patterns.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
     patterns.pack();
     patterns.setLocation(450, 500);
     patterns.setVisible(true);
   }
}


class ScoreBoardThread implements Runnable {
    
    static final int MATCH = 10;
    static final int MISMATCH = -8;
    static final int NEW_SPACE = -8;
    static final int EXTEND_SPACE = -1;
    
    static final int LEFT = 1;
    static final int UP = 2;
    static final int DIAG = 3;
    static final int ZERO = 0;
    
    private int[][] scoreBoard;
    private int[][] rowResults;
    private int[][] columnResults;
    private int[][] path;
    private int left, right, up, down;
    private final String str1;
    private final String str2;
    
    public ScoreBoardThread(int[][] scoreBoard, int[][] rowResults, int[][] columnResults,
            int[][] path, Bounds bounds, String str1, String str2) {
        this.scoreBoard = scoreBoard;
        this.rowResults = rowResults;
        this.columnResults = columnResults;
        this.path = path;
        this.left = bounds.left;
        this.right = bounds.right;
        this.up = bounds.up;
        this.down = bounds.down;
        this.str1 = str1;
        this.str2 = str2;
    }
    
    @Override
    public void run() {
        int i, j, upperLeft, max = 0, maxI = 0, maxJ = 0;
        
        for(i = up; i <= down; i++) {
            for(j = left; j <= right; j++) {
                rowResults[i][j] = Math.max(rowResults[i][j - 1] + EXTEND_SPACE, scoreBoard[i][j - 1] + NEW_SPACE);
                columnResults[i][j] = Math.max(columnResults[i - 1][j] + EXTEND_SPACE, scoreBoard[i - 1][j] + NEW_SPACE);
                
                upperLeft = scoreBoard[i - 1][j - 1] + compare(i, j);

                scoreBoard[i][j] = 0;
                path[i][j] = ZERO;
                
                if(rowResults[i][j] >= scoreBoard[i][j]) {
                    scoreBoard[i][j] = rowResults[i][j];
                    path[i][j] = LEFT;
                }
                if(columnResults[i][j] >= scoreBoard[i][j]) {
                    scoreBoard[i][j] = columnResults[i][j];
                    path[i][j] = UP;
                }
                if(upperLeft >= scoreBoard[i][j]) {
                    scoreBoard[i][j] = upperLeft;
                    path[i][j] = DIAG;
                }
            }
        }
    }
    
    private int compare(int i, int j) {
       if(str1.charAt(i - 1) == str2.charAt(j - 1)) {
           return MATCH;
       }
       return MISMATCH;
   }
}


class Sequence {
    
    public String str1;
    public String str2;
    public int score;
    
    public Sequence(String st1, String st2, int sc) {
        str1 = st1;
        str2 = st2;
        score = sc;
    }
}


class Bounds {
    
    public int up;
    public int down;
    public int left;
    public int right;
    
    public Bounds(int up, int down, int left, int right) {
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
    }
}